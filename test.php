<?php


namespace Local\Ajax\Catalog;


use Bitrix\Main\ArgumentException;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CIBlockElement;
use Local\Ajax\IAjax;
use Local\Catalog\BrandController;

class FindModels implements IAjax
{
    /**
     * @param HttpRequest $httpRequest
     * @return string
     * @throws ArgumentException
     * @throws LoaderException
     */
    function execute(HttpRequest $httpRequest): string
    {
        $processName = $this->getRequestProcess($httpRequest);
        $typeName = (string)$httpRequest->get('type');
        $subtypeName = (string)$httpRequest->get('subtype');
        $brandName = (string)$httpRequest->get('brand');
        $filter = $this->makeFilter($processName, $typeName, $subtypeName, $brandName);
        $modelList = $this->getModelList($filter, ['PROPERTY_MODEL' => 'ASC']);
        return json_encode($modelList, JSON_HEX_APOS);
    }

    /**
     * @param array $filter
     * @param array $sort
     * @return array
     * @throws LoaderException
     */
    private function getModelList(array $filter, $sort = []): array
    {
        Loader::includeModule('iblock');
        $dbRes =  CIBlockElement::GetList(
            $sort,
            $filter,
            false,
            ['nTopCount' => 300],
            [
                'ID',
                'IBLOCK',
                'PROPERTY_MODEL'
            ]
        );
        $modelList = [];
        while ($item = $dbRes->GetNext()) {
            if (mb_strlen($model = trim($item['PROPERTY_MODEL_VALUE']))) {
                $modelList[$model] = true;
            }
        }
        return array_keys($modelList);
    }

    /**
     * @param string $processName
     * @param string $typeName
     * @param string $subtypeName
     * @param string $brand
     * @return array
     * @throws LoaderException
     */
    public function makeFilter(string $processName, string $typeName, string $subtypeName, string $brand): array
    {
        $allowBrandList = (new BrandController())->getAllowBrandList();
        $brandsFilter = array_column($allowBrandList, 'NAME');
        if (in_array($brand, $brandsFilter)) {
            $brandsFilter = $brand;
        }
        $filter = [
            'IBLOCK_ID' => EQUIPMENT_IBLOCK_ID,
            'ACTIVE' => 'Y',
            'PROPERTY_PROCESS.NAME' => $processName,
            'PROPERTY_BRAND.NAME' => $brandsFilter,
        ];
        if (mb_strlen($typeName) && $typeName != 'null') {
            $filter['PROPERTY_EQUIPMENT_TYPE.NAME'] = $typeName;
        }
        if (mb_strlen($subtypeName) && $subtypeName != 'null') {
            $filter['PROPERTY_SUBTYPE.NAME'] = $subtypeName;
        }
        return $filter;
    }

    /**
     * @param HttpRequest $httpRequest
     * @return string
     * @throws ArgumentException
     */
    public function getRequestProcess(HttpRequest $httpRequest): string
    {
        if (!mb_strlen($process = (string)$httpRequest->get('process'))) {
            $process = (string)$httpRequest->get('equipment');
        }
        if (!mb_strlen($process) || $process == 'null') {
            throw new ArgumentException('Не передан процесс ' . $process);
        }
        return $process;
    }


}